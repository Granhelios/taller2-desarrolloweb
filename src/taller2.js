import React, { Fragment, useEffect, useState } from 'react'
import axios from 'axios'
import MaterialDatatable from "material-datatable";


const taller2 = () => {
    const [nombre, setNombre] = useState("");
    const [apellido, setApellido] = useState("");
    const [personas, setPersonas] = useState([]);
    const [idModificar, setIdModificar] = useState("");

    const handleInputChangeNombre = (event) => {
        
        setNombre(event.target.value);
    }

    const handleInputChangeApellido = (event) => {
        
        setApellido(event.target.value);

    }

    const enviarDatos = () => {
       
        console.log(`Enviando datos nombre:${nombre} y apellido:${apellido}`);

        guardarPersona();
     
        
    }


    useEffect(()=>{

        getPersonas()
    },[]);


    async function getPersonas() {
        try {
          const response = await axios.get('http://192.99.144.232:5000/api/personas?grupo=12');
          if(response.status == 200)
          {
            
            setPersonas(response.data.persona)
            console.log(response.data);


          }
         
        } catch (error) {
          console.error(error);
        }
      }
    
      function guardarPersona()
      {
        axios
        
        .post('http://192.99.144.232:5000/api/personas', {
            nombre: nombre,
            apellido: apellido,
            grupo: 12,
          })


        .then(function (response) {

                if(response.status==200)
                {
                    alert("Registro correcto");
                    getPersonas();

                }else{
                    alert("Error al guardar");
                }
            
          })
        .catch(function (error) {
            console.log(error);
          });
      }


      const editarPersona = () =>{
          axios
          .put(`http://192.99.144.232:5000/api/personas/${idModificar}`, {
              nombre: nombre,
              apellido: apellido,
          })

          .then(function (response){
              if (response.status === 200){
                  alert("Registro realizado correctamente");
                  setIdModificar("");
                  getPersonas();
              }
          })
      }

      const columns = [
        {
         nombre: "Nombre",
         field: "nombre",
         options: {
          filter: true,
          sort: true,
         }
        },
        {
         name: "Apellido",
         field: "apellido",
         options: {
          filter: true,
          sort: false,
         }
        },
       ];
        
      
       const handleRowClick = (rowData, rowMeta) => {
        console.log(rowData._id);
        setIdModificar(eorData._id);
        setNombre(rowData.nombre);
        setApellido(rowData.apellido);
    };
       const options = {
        filterType: 'checkbox',
        onlyOneRowCanBeSelected:true,
        onRowClick: handleRowClick
       };
      
    return (
        <Fragment>
            <h1>Formulario</h1>
            <div>
                <div>
                    <input type="text" placeholder="Nombre" name="nombre" onChange={handleInputChangeNombre} value={nombre} ></input>
                </div>

                <div>
                    <input type="text" placeholder="Apellido" name="apellido" onChange={handleInputChangeApellido} value={apellido}></input>
                </div>

                <button onClick={enviarDatos}>Enviar</button>



            </div>
                <MaterialDatatable
                    title={"Employee List"}
                    data={data}
                    columns={columns}
                    options={options}
                />

        </Fragment>

    )
}

export default taller2;